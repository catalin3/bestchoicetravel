import React, { Component } from 'react';
import './scss/App.scss';
import MainComponent from './components/MainComponent';

class App extends Component {
  render() {
    return (
      <MainComponent></MainComponent>
    );
  }
}

export default App;
