export const config = {
    apiUrl:"https://localhost:44371/api",
    apiUrlRecognition:"https://westeurope.api.cognitive.microsoft.com/vision/v2.0/recognizeText",
    restDbconfig: {
        headers: {
            "Content-Type":"application/octet-stream",
            "Ocp-Apim-Subscription-Key":"26590f54196947ff82151b02268a04f3"
        }
    },
    recognitionKey: "26590f54196947ff82151b02268a04f3"
}