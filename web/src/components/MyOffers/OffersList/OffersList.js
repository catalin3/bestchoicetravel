import React, { Component } from 'react';
import OfferService from '../../../services/OfferServices';
import PersonalOffer from '../../Models/PersonalOffer';

export default class OffersList extends Component {
    constructor(props){
        super(props);
        this.state = {
            offers:[],
            loading:true
        }
    }

    componentDidMount(){
        OfferService.getUserOffers(window.localStorage.getItem('agentId'))
            .then((result) => {
                this.setState({
                    offers:result,
                    loading:false
                });
            });
    }

    componentWillUpdate(){
        OfferService.getUserOffers(window.localStorage.getItem('agentId'))
            .then((result) => {
                this.setState({
                    offers:result,
                    loading:false
                });
            });
    }

    deleteOffer = (id) => {
        OfferService.deletePersonalOffer(id)
            .then(() => {
                this.setState((previousState) => ({
                    offers: [...previousState.offers.filter(m => m.id != id)]
                }));
            })
    }

    render() {
        return (
            <div>
                <table className="gr-table-standings">
                    <thead className="gr-head-standings">
                        <tr>
                            <th>Accommodation Type</th>
                            <th>Accommodation</th>
                            <th>Country</th>
                            <th>City</th>
                            <th>Price</th>
                            <th>Currency</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    

                    {this.state.loading && <h1>Loading</h1>}
                    {   
                        !this.state.loading && this.state.offers.map((item,index)=> (
                            <PersonalOffer 
                                key={index} 
                                item={item} 
                                deleteOffer={this.deleteOffer}
                            />
                        ))
                    }
                    </tbody>
                </table>
            </div>
        )
    }
}
