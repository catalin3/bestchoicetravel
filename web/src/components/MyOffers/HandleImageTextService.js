
export default class HandleImageTextService {
    
    static extractText = (offer) => {
        //console.log(offer);
        var jsonRegions = JSON.parse(JSON.stringify(offer));
        var words = new Array();
        
        var i = 0;
        for(;i<jsonRegions.regions.length;i++){
            //console.log(jsonRegions.regions[i].lines);
            //console.log(i+ " REGION############################################################")
            //i+=1;
            var j=0;
            for(;j<jsonRegions.regions[i].lines.length;j++){
                //console.log(jsonRegions.regions[i].lines[j].words);
                //console.log('LINE----------------------------------------------------------')
                //j+=1;
                var k=0;
                for(;k<jsonRegions.regions[i].lines[j].words.length;k++){
                    //console.log(jsonRegions.regions[i].lines[j].words[k].text);
                    words.push(jsonRegions.regions[i].lines[j].words[k].text)
                }
                words.push('|');
            }
            words.push('#');
        }
        return words;
    } 
}
