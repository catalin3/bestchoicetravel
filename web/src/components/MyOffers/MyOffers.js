import React, { Component } from 'react';
import HandleImageTextService from './HandleImageTextService';
import * as mongoose from 'mongoose';
import OfferService from '../../services/OfferServices';
import OffersList from './OffersList/OffersList';
import ProfileManager from '../Profile/ProfileManager/ProfileManager';

export default class MyOffers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedFile: null,
            image: null,
            textJsonFromImage: null,
            personalOfferList:[],
            errorMessage:''
        }
    }

    makeblob = (dataURL) => {
        var BASE64_MARKER = ';base64,';
        var parts = null;
        var contentType = null;
        var raw = null;
        if (dataURL.indexOf(BASE64_MARKER) === -1) {
            parts = dataURL.split(',');
            contentType = parts[0].split(':')[1];
            raw = decodeURIComponent(parts[1]);
            return new Blob([raw], { type: contentType });
        }
        else
            parts = dataURL.split(BASE64_MARKER);
        contentType = parts[0].split(':')[1];
        raw = window.atob(parts[1]);
        var rawLength = raw.length;

        var uInt8Array = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], { type: contentType });
    }

    fileSelectedHandler = (event) => {
        if(this.state.image == null){
            this.setState(
                {
                    errorMessage:"Please upload image"
                }
            )
        }
        else
        {
            this.setState(
                {
                    errorMessage:''
                }
            )
            var url = 'https://westeurope.api.cognitive.microsoft.com/vision/v1.0/ocr?language=unk&detectOrientation+=true';

            fetch(url, {
                method: 'POST',
                body: this.makeblob(this.state.image),
                headers: {
                    "Content-Type": "application/octet-stream",
                    "Ocp-Apim-Subscription-Key": "e7f7cbf3fb944955baeba802e2464c98"
                }
            }).then(res => res.json())
                .then(response => {
                    var words = HandleImageTextService.extractText(response);
                    this.extractOffer(words);
                })
                .catch(error => console.error('Error:', error));
        }
    }

    handleImage = (e) => {
        var reader = new FileReader();
        reader.onload = (e) => {
            let imageDataAsString = e.target.result;
            this.setState({
                image: imageDataAsString
            });
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    extractOffer = (words) => {
        var Accommodation = null;
        var AccommodationType = null;
        var Country = null;
        var City = null;
        var Agency = null;
        var Price = null;
        var Currency = null;
        console.log("CUVINTE: " + words);

        var NrRegion = 0;
        var NrLine = 0;
        for (var i = 0; i < words.length; i++) {
            if ("#" === words[i]) {
                NrRegion++;
                NrLine = 0;
                console.log(NrRegion);

            }
            else if ("|" === words[i]) {
                NrLine++;
            }
            else {
                if (NrRegion < 3) {
                    if (Agency === null) {
                        if (words[i].includes("TUR") || words[i].includes("tur") || words[i].includes("TRAVEL") || words[i].includes("travel")) {
                            Agency = words[i];
                        }
                    }
                    if (AccommodationType === null) {
                        
                        if (words[i].includes("Hotel") || words[i].includes("HOTEL") || words[i].includes("ote") || words[i].includes("Hote")) {
                            AccommodationType = "Hotel";
                        }
                    }
                    if (Accommodation === null && AccommodationType != null) {
                        console.log(words[i]);
                        Accommodation = "";
                        i++;
                        while (words[i] !== '#' && words[i] !== '|' && words[i] != '*'&& words[i] != '**'&& words[i] != '***'&& words[i] != '*****'&& words[i] != '*****') { 
                            Accommodation += " " + words[i];
                            i++;
                        }
                    }
                    if(NrRegion == 0){
                        if(Accommodation !== null && City === null && Country === null){
                            if(words[i+1] !== '#' && words[i+2] !== '|'){
                                
                            
                            City = words[i+1];
                            Country = words[i+2];
                            }
                        }
                    }
                    if(Accommodation !== null && City === null && Country === null && NrRegion == 2){
                        //if(words[i+1] !== '#' && words[i+2] !== '|'){
                        City = words[i];
                        Country = words[i+1];
                        //}
                    }
                   
                }
                if(Price === null && Currency === null && (words[i] === "EUR" || words[i] === "RON" || words[i] === "USD")){
                    Price = words[i-1];
                    Currency = words[i];
                }
            }
        }
        console.log("Accommodation " + Accommodation);
        console.log("AccommodationType " + AccommodationType);
        console.log("Country " + Country);
        console.log("City " + City);
        console.log("Agency " + Agency);
        console.log("Price " + Price);
        console.log("Crurrency " + Currency);
        var offer = {
            Id: String(mongoose.Types.ObjectId()),
            Agent: Agency,
            Type: "",
            AccommodationType: AccommodationType,
            Accommodation: Accommodation,
            Country: Country,
            City: City,
            Street: "",
            Number: "",
            Stars: "",
            Price: Price,
            Currency: Currency,
            NumberOfPersons: "",
            Description: "",
            User: window.localStorage.getItem("agentId")
        }
        //OfferService.addOffer(offer);
        OfferService.addPersonalOffer(offer);
    }



    render() {
        return (
            window.localStorage.getItem('token') == null ?
            (
                <ProfileManager></ProfileManager>
            ):
            <div>
                
                <h1>My Offers</h1>
                <div>
                    <br /><br /><br />
                    <input type="file"
                        name="image"
                        onChange={(e) => this.handleImage(e)}
                    />
                    {
                        this.state.errorMessage?
                        <div className="errorMessage">
                            {this.state.errorMessage}
                        </div>:
                        ''
                    }
                    <br /><br /><br />
                    <button className="gr-addOffer"
                        onClick={(e) => this.fileSelectedHandler(e)}
                    >Upload</button>
                </div>

                <OffersList/>
            </div>
        )
    }
}
