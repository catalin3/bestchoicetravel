import React, {Component} from 'react';
import Header from './Header/Header';
import Root from '../Root';

export default class MainComponent extends Component{

    render(){
        return(
            <div className="mainComponent">
                <Header/>
                <Root/>
            </div>
        );
    }
}