import React, { Component } from 'react'
import UserService from '../../services/UserService';

export default class Contact extends Component {
    constructor(props){
        super(props);

        this.state = {
            agents:[],
            loading:false,
        }
    }

    componentDidMount(){
        UserService.getAllUsers()
            .then((result) => {
                this.setState({
                    agents:result
                });
            });
    }

    render() {
        return (
            <div className="gr-standings-main">
                <table className="gr-table-standings">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Agency</th>
                            <th>Phone Number</th>
                            <th>Email</th>
                            <th>Country</th>
                            <th>City</th>
                        </tr>
                    </thead>
                    <tbody>
                {this.state.loading && <h3>Loading</h3>}
                {!this.state.loading &&
                    this.state.agents.map((item,index) => (
                        item.agency !== "null" && item.agency !== ""?
                        <tr key={item.id} className="">
                            <td>{item.firstName}</td>
                            <td>{item.lastName}</td>
                            <td>{item.agency}</td>
                            <td>{item.phoneNumber}</td>
                            <td>{item.email}</td>
                            <td>{item.country}</td>
                            <td>{item.city}</td>
                        </tr>:
                        ''
                    ))}

                </tbody>        
                </table>
            </div>
        )
    }
}
