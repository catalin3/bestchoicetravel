import React, { Component } from 'react'

export default class Offer extends Component {
    constructor(props) {
        super(props);
        this.state ={
            
        }
    }


    render() {
        return (
            
            <tr key={this.props.item.id}>
                <td><span onClick={() => {this.props.selectOffer(this.props.item)}}>{`${this.props.item.accommodationType}`}</span></td>
                <td>{`${this.props.item.accommodation}`}</td>
                <td>{`${this.props.item.stars}`}</td>
                <td>{`${this.props.item.country}`}</td>
                <td>{`${this.props.item.city}`}</td>
                <td>{`${this.props.item.price}`}</td>
            </tr>
        )
    }
}
