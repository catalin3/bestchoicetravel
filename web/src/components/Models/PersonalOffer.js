import React, { Component } from 'react'

export default class PersonalOffer extends Component {
    constructor(props) {
        super(props);
        this.state ={
            
        }
    }


    render() {
        return (
            
            <tr key={this.props.item.id}>
            <td>{`${this.props.item.accommodationType}`}</td>
            <td>{`${this.props.item.accommodation}`}</td>
            <td>{`${this.props.item.country}`}</td>
            <td>{`${this.props.item.city}`}</td>
            <td>{`${this.props.item.price}`}</td>
            <td>{`${this.props.item.currency}`}</td>
            <td><button className="gr-deleteOffer" onClick={() => this.props.deleteOffer(this.props.item.id)}>x</button></td>

        </tr>
        )
    }
}
