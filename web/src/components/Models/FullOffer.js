import React, { Component } from 'react'
import UserService from '../../services/UserService';

export default class FullOffer extends Component {
    constructor(props) {
        super(props);
        this.state ={
            agentFN:null,
            agentLN: null,
            agency:null,
            phoneNumber:null
        }
    }

    componentDidMount = () => {
        UserService.getUser(this.props.item.agent)
            .then((result) => {
                this.setState({
                    agentFN: result.firstName,
                    agentLN: result.lastName,
                    agency: result.agency,
                    phoneNumber: result.phoneNumber
                });
            });
    }
    
    handleShowOffer(e){
        this.props.handleShowOffer(null);
    }
    render() {
        return (
            <div className="fulloffer">
                <button className="gr-addOffer"
                    onClick={(e)=>this.handleShowOffer(e)}
                >BACK</button>
            <div key={this.props.item.id} className="gr-imageMoment">  
                <p className="paccommodation"><a className="pbold">Accommodation: </a>{`${this.props.item.accommodation}`}</p>
                <p><a className="pbold">Accommodation Type: </a>{`${this.props.item.accommodationType}`}</p>              
                <p><a className="pbold">Stars: </a>{`${this.props.item.stars}`}</p>
                <p><a className="pbold">Country: </a>{`${this.props.item.country}`}</p>
                <p><a className="pbold">City: </a>{`${this.props.item.city}`}</p>
                <p><a className="pbold">Sejur: </a>{`${this.props.item.sejur} days`}</p>
                <p><a className="pbold">Persons: </a>{`${this.props.item.numberOfPersons}`}</p>
                <p><a className="pbold">Price: </a>{`${this.props.item.price}`}</p>
                <p><a className="pbold">Currency: </a>{`${this.props.item.currency}`}</p>
                <p><a className="pbold">Agency: </a>{`${this.state.agency}`}</p>
                <p><a className="pbold">{`${this.state.agentFN} `}</a>{`${this.state.agentLN}`}</p>
                <p><a className="pbold">Phone Number: </a>{`${this.state.phoneNumber}`}</p>
                <p><a className="pbold">Description: </a>{`${this.props.item.description}`}</p>
                </div>
                <div className="gr-imagecontainer">
                    <div><img src={this.props.item.image1} /></div>
                    <div><img src={this.props.item.image4} /></div>
                    <div><img src={this.props.item.image3} /></div>
                    <div><img src={this.props.item.image2} /></div>
                    <div><img src={this.props.item.image5} /></div>
                </div>
            </div>
        )
    }
}
