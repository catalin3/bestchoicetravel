import React, { Component } from 'react'
import OfferService from '../../services/OfferServices';
import { Pagination } from 'react-bootstrap';
import Offer from './../Models/Offer';
import FullOffer from '../Models/FullOffer';

export default class Travel extends Component {
    constructor(props){
        super(props);
        this.state = {
            search:'',
            offerSelected:null,
            offers: [],
            loading: true,
            sizeOfPage: 20,
            firstPage: 0,
            lastPage: 0,
            curentPage: 0,
            nextPage: 0,
            prevPage: 0,
            property: "AccommodationType",
            sort: "Descending"
        }
    }
    
    search = (e) => {
        
        //else{
            OfferService.getNumberOfOffers()
                .then((response) =>{
                    console.log("number of elements: "+response);
                    this.setState({
                        lastPage: (~~(response/this.state.sizeOfPage))
                    });
                    console.log("Pages: "+this.state.lastPage);
                })
            OfferService.getOfferSearch(this.state.search).then((result) => {
                this.setState({
                    offers: result,
                    loading: false,
                    firstPage: 0,
                    nextPage: 1,
                    prevPage: -1,
                    curentPage: 0
                });
            });
        //}
    }

    componentDidMount(){
        this.setState({
            sizeOfPage:  20
        });
        OfferService.getNumberOfOffers()
            .then((response) =>{
                console.log("number of elements: "+response);
                this.setState({
                    lastPage: (~~(response/this.state.sizeOfPage))
                });
                console.log("Pages: "+this.state.lastPage);
            })
        OfferService.getOffersPage(0,this.state.sizeOfPage,this.state.property, this.state.sort).then((result) => {
            this.setState({
                offers: result,
                loading: false,
                firstPage: 0,
                nextPage: 1,
                prevPage: -1,
                curentPage: 0
            });
        });
    }

    selectOffer = (offer) => {
        this.setState({
            offerSelected: offer
        });
    }

    handleShowOffer = () => {
        this.setState({
            offerSelected: null
        });
    }

    handleChange = (e) => {
        this.setState({
            search: e.target.value
        });
    }

    render() {
        return (
            <div className="gr-standings-main">
                {
                    this.state.offerSelected != null?
                    <div>
                        <FullOffer
                            item={this.state.offerSelected}
                            handleShowOffer={this.handleShowOffer}
                        />
                    </div>:
                    <div>
                    <div className="gr-search">

                        <div>
                            <input type="text" name="searchString" placeholder="search" onChange={this.handleChange.bind(this)} value={this.state.search}/>
                            <button onClick={(e) => this.search(e)}></button>
                        </div>
                    </div>
                    <Pagination className="gr-standings-pagination-main">
                        <Pagination.First   className = "gr-pagination-elem first"
                                            onClick={() => this.changePage(
                                                this.state.firstPage,
                                                this.state.firstPage-1,
                                                this.state.firstPage,
                                                this.state.firstPage+1,   
                                                this.state.lastPage,
                                                this.state.sizeOfPage,
                                                this.state.property,
                                                this.state.sort
                                            )}
                        />
                        <Pagination.Prev    className = "gr-pagination-elem prev"
                                            onClick={() => this.changePage(
                                                this.state.firstPage,
                                                this.state.prevPage-1,
                                                this.state.prevPage,
                                                this.state.curentPage,   
                                                this.state.lastPage,
                                                this.state.sizeOfPage,
                                                this.state.property,
                                                this.state.sort
                                            )}
                        />
                        <Pagination.Item    className = "gr-pagination-elem"
                                            onClick={() => this.changePage(
                                                this.state.firstPage,
                                                this.state.firstPage-1,
                                                this.state.firstPage,
                                                this.state.firstPage+1,   
                                                this.state.lastPage,
                                                this.state.sizeOfPage,
                                                this.state.property,
                                                this.state.sort
                                            )}
                        >{this.state.firstPage}</Pagination.Item>
                        <Pagination.Ellipsis  className = "gr-pagination-elem ellipsis"/>
                                            
                        {
                            (~~(this.state.lastPage/2)) > 2?
                            <Pagination.Item    className = "gr-pagination-elem"
                                                onClick={() => this.changePage(
                                                    this.state.firstPage,
                                                    ~~(this.state.lastPage/2)-1,
                                                    ~~(this.state.lastPage/2),
                                                    ~~(this.state.lastPage/2)+1,   
                                                    this.state.lastPage,
                                                    this.state.sizeOfPage,
                                                    this.state.property,
                                                    this.state.sort
                                                )}
                            >{~~(this.state.lastPage/2)}</Pagination.Item>:
                            ''
                                                
                        }

                        <Pagination.Ellipsis  className = "gr-pagination-elem ellipsis"/>
                        <Pagination.Item    className = "gr-pagination-elem"
                                            onClick={() => this.changePage(
                                                this.state.firstPage,
                                                this.state.lastPage-1,
                                                this.state.lastPage,
                                                this.state.lastPage+1,   
                                                this.state.lastPage,
                                                this.state.sizeOfPage,
                                                this.state.property,
                                                this.state.sort
                                            )}
                        >{this.state.lastPage}</Pagination.Item>
                        <Pagination.Next    className = "gr-pagination-elem next"
                                            onClick={() => this.changePage(
                                                this.state.firstPage,
                                                this.state.curentPage,
                                                this.state.curentPage+1,
                                                this.state.nextPage+1,   
                                                this.state.lastPage,
                                                this.state.sizeOfPage,
                                                this.state.property,
                                                this.state.sort
                                            )}  
                            />
                        <Pagination.Last    className = "gr-pagination-elem last"
                                            onClick={() => this.changePage(
                                                this.state.firstPage,
                                                this.state.lastPage-1,
                                                this.state.lastPage,
                                                this.state.lastPage+1,   
                                                this.state.lastPage,
                                                this.state.sizeOfPage,
                                                this.state.property,
                                                this.state.sort
                                            )}
                        />
                    </Pagination>
                    <div>
                        {this.state.loading && <h1>Loading</h1>}
                    </div>
                    <table className="gr-table-standings">
                        <thead className="gr-head-standings">
                            <tr>
                                <th>AccommodationType<button onClick={() => this.changePropertySort(
                                                        this.state.firstPage,
                                                        this.state.firstPage-1,
                                                        this.state.firstPage,
                                                        this.state.firstPage+1,   
                                                        this.state.lastPage,
                                                        this.state.sizeOfPage,
                                                        "AccommodationType",
                                                        "Ascending"     
                                )}>{"<"}</button><button onClick={() => this.changePropertySort(
                                                        this.state.firstPage,
                                                        this.state.firstPage-1,
                                                        this.state.firstPage,
                                                        this.state.firstPage+1,   
                                                        this.state.lastPage,
                                                        this.state.sizeOfPage,
                                                        "AccommodationType",
                                                        "Descending"     
                                )}>{">"}</button></th>
                                <th>Accommodation<button onClick={() => this.changePropertySort(
                                                        this.state.firstPage,
                                                        this.state.firstPage-1,
                                                        this.state.firstPage,
                                                        this.state.firstPage+1,   
                                                        this.state.lastPage,
                                                        this.state.sizeOfPage,
                                                        "Accommodation",
                                                        "Ascending"     
                                )}>{"<"}</button><button onClick={() => this.changePropertySort(
                                                        this.state.firstPage,
                                                        this.state.firstPage-1,
                                                        this.state.firstPage,
                                                        this.state.firstPage+1,   
                                                        this.state.lastPage,
                                                        this.state.sizeOfPage,
                                                        "Accommodation",
                                                        "Descending"     
                                )}>{">"}</button></th>
                                <th>Stars<button onClick={() => this.changePropertySort(
                                                        this.state.firstPage,
                                                        this.state.firstPage-1,
                                                        this.state.firstPage,
                                                        this.state.firstPage+1,   
                                                        this.state.lastPage,
                                                        this.state.sizeOfPage,
                                                        "Stars",
                                                        "Ascending"     
                                )}>{"<"}</button><button onClick={() => this.changePropertySort(
                                                        this.state.firstPage,
                                                        this.state.firstPage-1,
                                                        this.state.firstPage,
                                                        this.state.firstPage+1,   
                                                        this.state.lastPage,
                                                        this.state.sizeOfPage,
                                                        "Stars",
                                                        "Descending"     
                                )}>{">"}</button></th>
                                <th>Country<button onClick={() => this.changePropertySort(
                                                        this.state.firstPage,
                                                        this.state.firstPage-1,
                                                        this.state.firstPage,
                                                        this.state.firstPage+1,   
                                                        this.state.lastPage,
                                                        this.state.sizeOfPage,
                                                        "Country",
                                                        "Ascending"     
                                )}>{"<"}</button><button onClick={() => this.changePropertySort(
                                                        this.state.firstPage,
                                                        this.state.firstPage-1,
                                                        this.state.firstPage,
                                                        this.state.firstPage+1,   
                                                        this.state.lastPage,
                                                        this.state.sizeOfPage,
                                                        "Country",
                                                        "Descending"     
                                )}>{">"}</button></th>
                                <th>City<button onClick={() => this.changePropertySort(
                                                        this.state.firstPage,
                                                        this.state.firstPage-1,
                                                        this.state.firstPage,
                                                        this.state.firstPage+1,   
                                                        this.state.lastPage,
                                                        this.state.sizeOfPage,
                                                        "City",
                                                        "Ascending"     
                                )}>{"<"}</button><button onClick={() => this.changePropertySort(
                                                        this.state.firstPage,
                                                        this.state.firstPage-1,
                                                        this.state.firstPage,
                                                        this.state.firstPage+1,   
                                                        this.state.lastPage,
                                                        this.state.sizeOfPage,
                                                        "City",
                                                        "Descending"     
                                )}>{">"}</button></th>
                                <th>Price<button onClick={() => this.changePropertySort(
                                                        this.state.firstPage,
                                                        this.state.firstPage-1,
                                                        this.state.firstPage,
                                                        this.state.firstPage+1,   
                                                        this.state.lastPage,
                                                        this.state.sizeOfPage,
                                                        "Price",
                                                        "Ascending"     
                                )}>{"<"}</button><button onClick={() => this.changePropertySort(
                                                        this.state.firstPage,
                                                        this.state.firstPage-1,
                                                        this.state.firstPage,
                                                        this.state.firstPage+1,   
                                                        this.state.lastPage,
                                                        this.state.sizeOfPage,
                                                        "Price",
                                                        "Descending"     
                                )}>{">"}</button></th>
                            </tr>
                        </thead>
                        <tbody>
                                

                        {!this.state.loading &&
                            this.state.offers.map((item, index) => (
                                <Offer
                                    key={index}
                                    item={item}
                                    selectOffer={this.selectOffer}
                                >
                                </Offer>
                            )
                            )}
                        </tbody>
                    </table>
                    </div>
                }
            </div>
        )
                        
    }

    changePage =(firstPage, prevPage, curentPage, nextPage, lastPage, sizeOfPage, property, sort) => {
        if(curentPage >= firstPage && curentPage <= lastPage){
            OfferService.getOffersPage(curentPage, sizeOfPage, property, sort).then((result) => {
                this.setState({
                    offers: result,
                    loading: false,
                    firstPage: firstPage,
                    prevPage: prevPage,
                    curentPage: curentPage,
                    nextPage: nextPage,
                    lastPage: lastPage
                })
            })
        }
    }

    changePropertySort = (firstPage, prevPage, curentPage, nextPage, lastPage, sizeOfPage, property, sort) => {
        this.setState({
            property: property,
            sort: sort
        })
        this.changePage(firstPage,firstPage-1,firstPage,firstPage+1,lastPage,sizeOfPage,property,sort);
    }
}
