import React, { Component } from 'react';
import Login from '../../Login/Login';
import Register from '../../Register/Register';
import Profile from '../Profile';

class ProfileManager extends Component {
    constructor(props) {
        super(props);

        this.state = {
            profile: false,
            showRegisterComponent: false,
            token: ''
        }
    }

    handleShowRegisterComponent = (e) => {
        this.setState({
            showRegisterComponent: e
        });
    }

    handleRegister = (register) => {
        this.setState({
            showRegisterComponent: false
        });
    }

    handleSetToken = (token) => {
        this.setState({
            token: token
        });
    }

    render() {
        return (
            !this.state.token && window.localStorage.getItem('token') == null ?
                (
                    this.state.showRegisterComponent === true ?
                        <Register handleRegister={this.handleRegister} /> :
                        <Login handleSetToken={this.handleSetToken} handleShowRegisterComponent={this.handleShowRegisterComponent} />
                )
                :
                <Profile handleSetToken={this.handleSetToken} />
        )
    }

}

export default ProfileManager;