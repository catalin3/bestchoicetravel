import React,{Component} from 'react';
import AgentProfile from './AgentProfile/AgentProfile';
import UserProfile from './UserProfile/UserProfile';

class Profile extends Component{
    constructor(props){
        super(props);
        this.state = {
            profile: false
        }
    }

    handleLogout(e){
        window.localStorage.removeItem('token');
        window.localStorage.removeItem('username');
        window.localStorage.removeItem('agency');
        window.localStorage.removeItem('agentId');
        this.props.handleSetToken('');
    }
    render(){
        return(
            <div>
                <p>{"Logged "+ window.localStorage.getItem('username')}</p>
                <button className="gr-addOffer"
                    onClick={(e)=>this.handleLogout(e)}
                >Logout</button>
            {
                localStorage.getItem('agency')!==null && localStorage.getItem('agency')!==''?
                <AgentProfile/>:
                <UserProfile></UserProfile>
            
            }
            </div>
        );
    }
}

export default Profile;