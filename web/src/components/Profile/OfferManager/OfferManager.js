import React, { Component } from 'react'
import OfferService from '../../../services/OfferServices';
import * as mongoose from 'mongoose';

export default class OfferManager extends Component {
    constructor(props){
        super(props);
        this.state = {
            errorMessage:false,
            image1:null,
            image2:null,
            image3:null,
            image4:null,
            image5:null
        }
        this.typeRef = React.createRef();
        this.accommodationTypeRef = React.createRef();
        this.accommodationRef = React.createRef();
        this.starsRef = React.createRef();
        this.countryRef = React.createRef();
        this.cityRef = React.createRef();
        this.streetRef = React.createRef();
        this.numberRef = React.createRef();
        this.priceRef = React.createRef();
        this.numberOfPersonsRef = React.createRef();
        this.descriptionRef = React.createRef();
        this.currencyRef = React.createRef();
        this.sejurRef = React.createRef();
    }

    

    addOffer = (e) => {
        if(this.validate()){
            var offer = {
                Id: String(mongoose.Types.ObjectId()),
                Agent: localStorage.getItem("agentId"),
                Type: this.typeRef.current.value,
                AccommodationType: this.accommodationTypeRef.current.value,
                Accommodation: this.accommodationRef.current.value,
                Country: this.countryRef.current.value,
                City: this.cityRef.current.value,
                Street: this.streetRef.current.value,
                Number: this.numberRef.current.value,
                Stars: this.starsRef.current.value,
                Price: this.priceRef.current.value,
                NumberOfPersons: this.numberOfPersonsRef.current.value,
                Description: this.descriptionRef.current.value,
                Currency: this.currencyRef.current.value,
                Sejur: this.sejurRef.current.value,
                Image1: this.state.image1,
                Image2: this.state.image2,
                Image3: this.state.image3,
                Image4: this.state.image4,
                Image5: this.state.image5
            }
        
            OfferService.addOffer(offer)
                .then(result => {
                console.log(result)
                if(!result){
                    this.setState({
                        errorMessage:'Error to add Offer!'
                    });
                    return false;
                }
                this.props.history.push('/profilemanager');
            })
        }
    }


    render() {
        return (
                <div>
                    <div className="gr-offerform">
                        <h1>Add Offer</h1>
                        <input  type="text" name="type" ref={this.typeRef} placeholder={"Type"}/><br/>
                        <input  type="text" name="type" ref={this.accommodationTypeRef} placeholder={"Type of Accommodation"}/><br/>
                        <input  type="text" name="type" ref={this.accommodationRef} placeholder={"Accommodation"}/><br/>
                        <input  type="text" name="type" ref={this.starsRef} placeholder={"Stars"}/><br/>
                        <input  type="text" name="type" ref={this.countryRef} placeholder={"Country"}/><br/>
                        <input  type="text" name="type" ref={this.cityRef} placeholder={"City"}/><br/>
                        <input  type="text" name="type" ref={this.streetRef} placeholder={"Street"}/><br/>
                        <input  type="text" name="type" ref={this.numberRef} placeholder={"Nr"}/><br/>
                        <input  type="text" name="type" ref={this.numberOfPersonsRef} placeholder={"Persons"}/><br/>
                        <input  type="text" name="type" ref={this.sejurRef} placeholder={"Sejur"}/><br/>
                        <input  type="text" name="type" ref={this.priceRef} placeholder={"Price"}/><br/>
                        <input  type="text" name="type" ref={this.currencyRef} placeholder={"Currency"}/><br/>
                        <textarea  type="text" name="type" ref={this.descriptionRef} placeholder={"Description"} rows="4" cols="50"/><br/>
                        <input 
                            id="imaginetare1"
                            type="file" 
                            name="image1" 
                            onChange={(e)=>this.handleImage1(e)}  
                        /><br/>
                        <input 
                            id="imaginetare2"
                            type="file" 
                            name="image2" 
                            onChange={(e)=>this.handleImage2(e)}  
                        /><br/>
                        <input 
                            id="imaginetare3"
                            type="file" 
                            name="image3" 
                            onChange={(e)=>this.handleImage3(e)}  
                        /><br/>
                        <input 
                            id="imaginetare4"
                            type="file" 
                            name="image4" 
                            onChange={(e)=>this.handleImage4(e)}  
                        /><br/>
                        <input 
                            id="imaginetare5"
                            type="file" 
                            name="image5" 
                            onChange={(e)=>this.handleImage5(e)}  
                        /><br/>
                        <button className="gr-addOffer"
                            onClick = {(e) => this.addOffer(e)}
                        >Add Offer</button>
                        {
                            this.state.errorMessage ?
                            <div>
                                {this.state.errorMessage}
                            </div>:
                            ''
                        }
                    </div>
                    
                    
                   
                </div>
        )
    }

    validate = () => {
        if(this.typeRef.current.value === ''){
            this.setState({
                errorMessage: 'Insert Type!'
            });
            return false;
        }
        if(this.accommodationTypeRef.current.value === ''){
            this.setState({
                errorMessage: 'Insert AccommodationType!'
            });
            return false;
        }
        if(this.starsRef.current.value === ''){
            this.setState({
                errorMessage: 'Insert Stars!'
            });
            return false;
        }
        if(this.countryRef.current.value === ''){
            this.setState({
                errorMessage: 'Insert Country!'
            });
            return false;
        }
        if(this.cityRef.current.value === ''){
            this.setState({
                errorMessage: 'Insert City!'
            });
            return false;
        }
        if(this.streetRef.current.value === ''){
            this.setState({
                errorMessage: 'Insert Street!'
            });
            return false;
        }
        if(this.numberRef.current.value === ''){
            this.setState({
                errorMessage: 'Insert Number!'
            });
            return false;
        }
        if(this.numberOfPersonsRef.current.value === ''){
            this.setState({
                errorMessage: 'Insert number of persons!'
            });
            return false;
        }
        if(this.priceRef.current.value === ''){
            this.setState({
                errorMessage: 'Insert price!'
            });
            return false;
        }
        if(this.currencyRef.current.value === ''){
            this.setState({
                errorMessage: 'Insert currency!'
            });
            return false;
        }
        return true;
    }

    initilizeParametters(){
        this.typeRef = "";
        this.accommodationTypeRef = "";
        this.accommodationRef = "";
        this.starsRef = "";
        this.countryRef = "";
        this.cityRef = "";
        this.streetRef = "";
        this.numberRef = "";
        this.priceRef = "";
        this.numberOfPersonsRef = "";
        this.descriptionRef = "";
    }

    handleImage1 = (e) => {
        var reader = new FileReader();
        reader.onload = (e) => {
            let imageDataAsString = e.target.result;
            this.setState({
                image1: imageDataAsString,
            })
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    handleImage2 = (e) => {
        var reader = new FileReader();
        reader.onload = (e) => {
            let imageDataAsString = e.target.result;
            this.setState({
                image2: imageDataAsString,
            })
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    handleImage3 = (e) => {
        var reader = new FileReader();
        reader.onload = (e) => {
            let imageDataAsString = e.target.result;
            this.setState({
                image3: imageDataAsString,
            })
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    handleImage4 = (e) => {
        var reader = new FileReader();
        reader.onload = (e) => {
            let imageDataAsString = e.target.result;
            this.setState({
                image4: imageDataAsString,
            })
        }
        reader.readAsDataURL(e.target.files[0]);
    }

    handleImage5 = (e) => {
        var reader = new FileReader();
        reader.onload = (e) => {
            let imageDataAsString = e.target.result;
            this.setState({
                image5: imageDataAsString,
            })
        }
        reader.readAsDataURL(e.target.files[0]);
    }
}
