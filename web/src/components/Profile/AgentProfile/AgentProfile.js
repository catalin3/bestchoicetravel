import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import OfferService from '../../../services/OfferServices';
import AgentOffer from '../../Models/AgentOffer';
import ModifyManager from '../OfferManager/ModifyManager';

export default class AgentProfile extends Component {
    constructor(props){
        super(props);
        this.state = {
            offers:[],
            loading:true,
            modifyOffer:null
        }
    }

    componentDidMount(){
        OfferService.getAgentOffers(window.localStorage.getItem('agentId'))
            .then((result) => {
                this.setState({
                    offers:result,
                    loading:false
                });
            });
    }

    deleteOffer = (id) => {
        OfferService.deleteOffer(id)
            .then(() => {
                this.setState((previousState) => ({
                    offers: [...previousState.offers.filter(m => m.id !== id)]
                }));
            })
    }

    modifyOffer = (offer) => {
        this.setState({
            modifyOffer: offer
           //offers: [...previousState.offers.filter(m => m.id !== id)]
        });
    }

    handleShowModifyManager = () => {
        this.setState({
            modifyOffer: null
        });
        this.componentDidMount();
    }

    render() {
        return (
            <div>
                <div className="div-button-addOffer">
                    <Link className="button-addOffer" to='/addOffer'>Add Offer</Link>
                </div>
                {
                this.state.modifyOffer != null?
                <ModifyManager
                    item={this.state.modifyOffer}
                    handleShowModifyManager={this.handleShowModifyManager}
                >

                </ModifyManager>:
                <table className="gr-table-standings">
                    <thead className="gr-head-standings">
                        <tr>
                            <th>Accommodation Type</th>
                            <th>Accommodation</th>
                            <th>Stars</th>
                            <th>Country</th>
                            <th>City</th>
                            <th>Price</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    

                    {this.state.loading && <h1>Loading</h1>}
                    {   
                        !this.state.loading && this.state.offers.map((item,index)=> (
                            <AgentOffer 
                                key={index} 
                                item={item} 
                                deleteOffer={this.deleteOffer}
                                modifyOffer={this.modifyOffer}
                            />
                        ))
                    }
                    </tbody>
                </table>
                }
            </div>
        )
    }
}
