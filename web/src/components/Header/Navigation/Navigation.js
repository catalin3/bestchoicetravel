import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Navigation extends Component {

    render() {
        return (
            <div className="topnav" id="myTopnav">

                <Link to="/home" className="link">Home</Link>

                <Link to="/travel" className="link">Travel</Link>
                <Link to="/myOffer" className="link">My Offer</Link>
                <Link to="/contact" className="link">Contact</Link>
                <Link to="/profilemanager" className="link">Profile</Link>
                <img src={require('../fifth.png')} alt="game room logo"/>
            </div>
        );
    }
}
export default Navigation;