import React, { Component } from 'react';
import UserService from '../../services/UserService';

export default class Register extends Component {
    constructor(props){
        super(props);

        this.state = {
            errorMessage:'',
            isAgent:false
        }

        this.handleChange = this.handleChange.bind(this);
        this.usernameRef = React.createRef();
        this.fisrtNameRef = React.createRef();
        this.lasNameRef = React.createRef();
        this.passwordRef = React.createRef();
        this.retypePasswordRef = React.createRef();
        this.emailRef = React.createRef();
        this.phoneNumberRef = React.createRef();
        this.agencyRef = React.createRef();
        this.countryRef = React.createRef();
        this.cityRef = React.createRef();
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    isAgentValidation = (e) => {
        var showAgentMenu = !this.state.isAgent;
        console.log(showAgentMenu);
        this.setState({
            isAgent:showAgentMenu
        });
    }

    register = (e) => {
        if(this.state.isAgent){
            this.registerAgent(e);
        }
        else{
            this.registerUser(e);
        }
    }

    registerUser = (e) => {
        if(this.validate()){
            var user = {
                "Username":this.usernameRef.current.value,
                "Password":this.passwordRef.current.value,
                "FirstName":this.fisrtNameRef.current.value,
                "LastName":this.lasNameRef.current.value,
                "Email":this.emailRef.current.value,
                "PhoneNumber":"",
                "Agency":"",
                "Country":"",
                "City":""
            }
            UserService.addUser(user)
                .then(result => {
                    if(!result){
                        this.setState({
                            errorMessage: 'Error to add User!!'
                        })
                        return false;
                    }
                    this.props.handleRegister();
            });
        }
    }

    registerAgent = (e) => {
        if(this.validate()){
            var agent = {
                "Username":this.usernameRef.current.value,
                "Password":this.passwordRef.current.value,
                "FirstName":this.fisrtNameRef.current.value,
                "LastName":this.lasNameRef.current.value,
                "Email":this.emailRef.current.value,
                "PhoneNumber":this.phoneNumberRef.current.value,
                "Agency":this.agencyRef.current.value,
                "Country":this.countryRef.current.value,
                "City":this.cityRef.current.value
            }
            UserService.addUser(agent)
                .then(result => {
                    if(!result){
                        this.setState({
                            errorMessage: 'Error to add User!!'
                        })
                        return false;
                    }
                    this.props.handleRegister();
            });
        }
    }

    render() {
            return (
                <div className="gr-registermenu">
                    <h1>Register</h1>
                    <input type="text" name="username" ref={this.usernameRef} placeholder={"Username"}/><br/>
                    <input type="text" name="firstName" ref={this.fisrtNameRef} placeholder={"First Name"}/><br/>
                    <input type="text" name="lastName" ref={this.lasNameRef} placeholder={"Last Name"}/><br/>
                    <input type="password" name="password" ref={this.passwordRef} placeholder={"Password"}/><br/>
                    <input type="password" name="passwordR" ref={this.retypePasswordRef} placeholder={"Retype Password"}/><br/>
                    <input type="text" name="email" ref={this.emailRef} placeholder={"Email"}/><br/>
                    <button className="gr-addOffer"
                        onClick={(e) => this.isAgentValidation(e)}
                    >Are you an travel agent or agency?</button>
                    {
                        this.state.isAgent===true ?
                        <div>
                            <input type="text" name="phoneNumber" ref={this.phoneNumberRef} placeholder={"Phone Number"}/><br/>
                            <input type="text" name="agency" ref={this.agencyRef} placeholder={"Agency"}/><br/>
                            <input type="text" name="country" ref={this.countryRef} placeholder={"Country"}/><br/>
                            <input type="text" name="city" ref={this.cityRef} placeholder={"City"}/><br/>
                        </div>
                        :
                        ''
                    }
                    <br/>
                    <button className="gr-addOffer"
                        onClick={(e)=>this.register(e)}
                    >Register</button>
                    {
                        this.state.errorMessage?
                        <div className="errorMessage">
                            {this.state.errorMessage}
                        </div>:
                        ''
                    }
                </div>
            )
    }

    validate = () => {
        if(this.usernameRef.current.value === '' || this.usernameRef.current.value == null){
            this.setState({
                errorMessage:'Insert username!'
            });
            return false;
        }
        if(this.passwordRef.current.value === ''){
            this.setState({
                errorMessage:'Insert password!'
            });
            return false;
        }
        if(this.retypePasswordRef.current.value === ''){
            this.setState({
                errorMessage:'Retype password!'
            });
            return false;
        }
        
        if(this.emailRef.current.value === ''){
            this.setState({
                errorMessage:'Insert email!'
            });
            return false;
        }
        
        if(this.state.isAgent){
            if(this.agencyRef.current.value === ''){
                this.setState({
                    errorMessage:'Insert agency!'
                });
                return false;
            }
            if(this.cityRef.current.value === ''){
                this.setState({
                    errorMessage:'Insert city!'
                });
                return false;
            }
            if(this.countryRef.current.value === ''){
                this.setState({
                    errorMessage:'Insert country!'
                });
                return false;
            }
        }
        return true;
    }
}
