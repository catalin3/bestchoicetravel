import React,{Component} from 'react';
import UserService from '../../services/UserService';

class Login extends Component{
    constructor(props){
        super(props);
        this.state = {
            errorMessage:false
        }
        this.handleChange = this.handleChange.bind(this);
        this.usernameRef = React.createRef();
        this.passwordRef = React.createRef();
    }

    handleRegister(e){
        this.props.handleShowRegisterComponent(true);
    }

    login = (e) => {
        if(this.validate()){
            console.log(this.usernameRef.current.value);
            UserService.login(this.usernameRef.current.value, this.passwordRef.current.value)
                .then(result => {
                    console.log(result);
                    if(!result){
                        this.setState({
                            errorMessage:'Invalid username or password!'
                        });
                        return false;
                    }
                    window.localStorage.setItem('token',result.token);
                    console.log('Logged in token: '+result.token)

                    window.localStorage.setItem('username',result.username);
                    console.log('Logged in username: '+result.username)

                    window.localStorage.setItem('agency',result.agency);
                    console.log('Logged in agency: '+result.agency)

                    window.localStorage.setItem('agentId',result.id);
                    console.log('Agent: '+result.id)

                    this.props.handleSetToken(result.token);
                });
        }
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    

    render(){
        return(
            <div className="gr-logindiv">
                <h1>Login</h1>
                <div className="login-unsername">
                    <input type="text" name="username" ref={this.usernameRef} placeholder={"Username"}/>
                </div>
                <div className="login-password">
                    <input type="password" name="password" ref={this.passwordRef} placeholder={"********"}/>
                </div>
                <button 
                    className="gr-addOffer"
                    onClick={(e)=>this.login(e)}
                >
                    Login
                </button>
                {
                    this.state.errorMessage ?
                    <div>
                        {this.state.errorMessage}
                    </div>
                    :
                    ''
                }
                <p>You don't have an account? Please    <button className="registerButton"
                                                            onClick = {(e)=>this.handleRegister(e)} 
                                                            >Register 
                                                        </button></p>
            </div>
        );
    }

    validate = () => {
        if(this.usernameRef.current.value === ''){
            this.setState({
                errorMessage:'Insert username!'
            });
            return false;
        }
        if(this.passwordRef.current.value === ''){
            this.setState({
                errorMessage:'Insert password!'
            });
            return false;
        }
        return true;
    }
}

export default Login;