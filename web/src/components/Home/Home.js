import React,{Component} from 'react';
import CarouselSlider from './Carousel/CarouselSlider';
import OfferNews from './OfferNews/OfferNews';
import TopAccommodations from './TopAccommodations/TopAccommodations';

class Home extends Component{
    render(){
        return(
            <div className="gr-home">
                <CarouselSlider></CarouselSlider>
                <OfferNews></OfferNews>
                <TopAccommodations></TopAccommodations>
            </div>
        );
    }
}

export default Home;