import React, { Component } from 'react';
import Slider from 'react-slick';
//import '../../../scss/components/Home/Carousel/CarouselSlider/_carouselSlider.scss';

export default class CarouselSlider extends Component {


    render() {
        var settings = {
            dots: true,
            infinite: true,
            speed: 7000,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
        };
        return (
            <div className="gr-carousel-slider">
                <Slider {...settings}>
                    <div className="carousel-logo">
                        <img src={require('./CarouselImages/maldive.jpg')} alt="game room logo" />
                    </div>
                    <div>
                        <img src={require('./CarouselImages/austria.jpg')} alt="game room logo" />
                    </div>
                    <div>
                        <img src={require('./CarouselImages/cairo.jpg')} alt="game room logo" />
                    </div>
                    <div>
                        <img src={require('./CarouselImages/singapore.jpg')} alt="game room logo" />
                    </div>
                    <div>
                        <img src={require('./CarouselImages/cruize.jpg')} alt="game room logo" />
                    </div>

                </Slider>
            </div>
        );

    }
}
