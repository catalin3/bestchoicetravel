import React, { Component } from 'react';
import OfferService from '../../../services/OfferServices';

export default class TopAccommodations extends Component {
    constructor(props){
        super(props);
        this.state = {
            offers:[],
            loading:false,
        }
    }

    componentDidMount(){
        OfferService.getTop10Offers().then((result) => {
            this.setState({
                offers: result
            });
        });
    }
    render() {
        return (
            <div className="gr-momentnews">
                <h2>Top Accommodations</h2>
                {this.state.loading && <h3>Loading</h3>}
                
                {!this.state.loading &&
                    this.state.offers.map((item,index) => (
                        <div key={item.id} className="gr-offernew">
                            <img src={item.image1} />
                            <p>{item.accommodation}</p>
                               
                        </div>
                    ))}
            </div>
        )
    }
}
