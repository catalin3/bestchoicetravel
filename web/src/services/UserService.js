import { config } from '../helpers/config';

const axios = require('axios');
export default class UserService {

    static login = (username, password) => {
        var user = {
            "Username":username,
            "Password":password
        }
        return axios
                .post(`${config.apiUrl}/user`, user)
                .then((result) => result.data)
                .catch((error) => false);
    }

    static addUser = (user) => {
        return axios.post(`${config.apiUrl}/usercrud`,user)
            .then((result) => result.data)
            .catch((error) => false);
    }

    static getUser = (id) => {
        return axios.get(`${config.apiUrl}/usercrud?Id=${id}`)
        .then((result) => result.data)
        .catch((error) => false);
    }

    static getAllUsers = (id) => {
        return axios.get(`${config.apiUrl}/usercrud`)
        .then((result) => result.data)
        .catch((error) => false);
    }
    
}
