import { config } from '../helpers/config';

const axios = require('axios');
export default class OfferService {
    
    static addOffer = (offer) => {
        return axios
            .post(`${config.apiUrl}/offer`,offer)
            .then((result) => result.data)
            .catch((error) => false);
    }

    static addPersonalOffer = (offer) => {
        return axios
            .post(`${config.apiUrl}/personalOfferList`,offer)
            .then((result) => result.data)
            .catch((error) => false);
    }

    static getAllOffers = () => {
        return axios
            .get(`${config.apiUrl}/offer`)
            .then((result)=> result.data)
            .catch((error)=> false)
    }

    static getOfferSearch = (search) => {
        return axios
            .get(`${config.apiUrl}/offer?Search=${search}`)
            .then((result)=> result.data)
            .catch((error)=> false)
    }

    static getLast10Offers = () => {
        return axios
        .get(`${config.apiUrl}/offer?Last=${"true"}`)
        .then((result)=> result.data)
        .catch((error)=> false)
    }

    static getTop10Offers = () => {
        return axios
        .get(`${config.apiUrl}/offer?Last=${"top"}`)
        .then((result)=> result.data)
        .catch((error)=> false)
    }

    static getAgentOffers = (agent) => {
        return axios
            .get(`${config.apiUrl}/offer?Agent=${agent}`)
            .then((result)=> result.data)
            .catch((error)=> false)
    }

    static getUserOffers = (agent) => {
        return axios
            .get(`${config.apiUrl}/personalofferlist?User=${agent}`)
            .then((result)=> result.data)
            .catch((error)=> false)
    }

    static getOffersPage = (pageNumber, pageSize, property, sort) => {
        return axios
            .get(`${config.apiUrl}/offer?PageNumber=${pageNumber}&PageSize=${pageSize}&Property=${property}&sort=${sort}`)
            .then((result)=> result.data)
            .catch((error)=> false);
    }

    static getNumberOfOffers = () => {
        return axios
            .get(`${config.apiUrl}/offer?Count=${"true"}`)
            .then((result) => result.data);
    }

    static deleteOffer = (offerId) => {
        return axios 
                .delete(`${config.apiUrl}/offer/${offerId}`)
                .then((result) => result.data)
                .catch((error)=> false);
    }

    static deletePersonalOffer = (offerId) => {
        return axios 
                .delete(`${config.apiUrl}/personalOfferList/${offerId}`)
                .then((result) => result.data)
                .catch((error)=> false);
    }

    static modifyOffer = (id,newOffer) => {
        return axios
                .put(`${config.apiUrl}/offer/${id}`,newOffer)
                .then((result) => result.data)
                .catch((error) => false);
    }
    
}
