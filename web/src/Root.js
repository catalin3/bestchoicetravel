import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Home from './components/Home/Home';
import Login from './components/Login/Login';
import ProfileManager from './components/Profile/ProfileManager/ProfileManager';
import MyOffers from './components/MyOffers/MyOffers';
import OfferManager from './components/Profile/OfferManager/OfferManager';
import Travel from './components/Travel/Travel';
import Contact from './components/Contact/Contact';

const Root = () => (
    <main>
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/home" component={Home}/>
            <Route exact path="/travel" component={Travel}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/profilemanager" component={ProfileManager}/>
            <Route exact path="/myoffer" component={MyOffers}/>
            <Route exact path="/addOffer" component={OfferManager}/>
            <Route exact path="/contact" component={Contact}/>
        </Switch>
    </main>
);

export default Root;