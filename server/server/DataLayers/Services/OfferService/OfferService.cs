﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using server.DataLayers.Models;
using server.DataLayers.Repositories.OfferRepo;

namespace server.DataLayers.Services.OfferService
{
    public class OfferService : IOfferService
    {
        private readonly IOfferRepository _offerRepository;

        public OfferService(IOfferRepository offerRepository)
        {
            this._offerRepository = offerRepository;
        }

        public IEnumerable<Offer> FindAll()
        {
            return _offerRepository.FindAll();
        }

        public Offer FindById(string id)
        {
            return _offerRepository.FindById(id);
        }

        public int GetNumberOfOffers()
        {
            return _offerRepository.GetNumberOfOffers();
        }

        public IEnumerable<Offer> GetOffersByAgent(string agent)
        {
            return _offerRepository.GetOffersByAgent(agent);
        }

        public IEnumerable<Offer> GetOffersByUser(string user)
        {
            return _offerRepository.GetOffersByUser(user);
        }

        public Offer Insert(Offer t)
        {
            return _offerRepository.Insert(t);
        }

        public IEnumerable<Offer> PaginationFind(int pageNumber, int pageSize)
        {
            return _offerRepository.PaginationFind(pageNumber, pageSize);
        }

        public IEnumerable<Offer> GetLast10()
        {
            var last10 = new List<Offer>();
            if (GetNumberOfOffers() < 10) { return FindAll(); }
            int pages = GetNumberOfOffers() / 10;
            pages--;
            if (GetNumberOfOffers() % 10 != 0) { pages++; }
            var pageOfImages = PaginationFind(pages, 10);

            foreach (var image in pageOfImages)
            {
                last10.Add(image);
                if (last10.Count() == 10)
                {
                    return last10;
                }
            }

            var pageOfImages2 = PaginationFind(pages - 1, 10);

            foreach (var image in pageOfImages2)
            {
                last10.Add(image);
                if (last10.Count() == 10)
                {
                    return last10;
                }
            }

            return last10;
        }

        public IEnumerable<Offer> GetTop10()
        {
            var last10 = new List<Offer>();
            if (GetNumberOfOffers() <= 10) { return FindAll(); }
            int pages = GetNumberOfOffers() / 10;
            pages--;
            if (GetNumberOfOffers() % 10 != 0) { pages++; }
            var pageOfImages = PaginationFindAll(pages, 10, "Stars", "Descending");

            foreach (var image in pageOfImages)
            {
                last10.Add(image);
                if (last10.Count() == 10)
                {
                    return last10;
                }
            }

            var pageOfImages2 = PaginationFindAll(pages - 1, 10, "Stars", "Descending");

            foreach (var image in pageOfImages2)
            {
                last10.Add(image);
                if (last10.Count() == 10)
                {
                    return last10;
                }
            }

            return last10;
        }

        public IEnumerable<Offer> PaginationFindAll(int pageNumber, int pageSize, string property, string sort)
        {
            return _offerRepository.PaginationFindAll(pageNumber, pageSize, property, sort);
        }

        public void Remove(string id)
        {
            _offerRepository.Remove(id);
        }

        public Offer Update(string id, Offer t)
        {
            return _offerRepository.Update(id, t);
        }

        public IEnumerable<Offer> GetOfferBySearch(string search)
        {
            List<Offer> offers = (List<Offer>)FindAll();
            for(int i = 0; i < offers.Count; i++)
            {
                Boolean valid = false;
                if (offers[i].Type != null && offers[i].AccommodationType != null && offers[i].Accommodation != null && offers[i].Country != null && offers[i].City != null && offers[i].Currency != null)
                {
                    if (offers[i].Type.Contains(search) || offers[i].AccommodationType.Contains(search) || offers[i].Accommodation.Contains(search) || offers[i].Country.Contains(search) || offers[i].City.Contains(search) || offers[i].Currency.Contains(search))
                    {
                        valid = true;
                    }
                }
                if(valid == false)
                {
                    offers.RemoveAt(i);
                    i--;
                }
            }
            return offers;
        }
    }
}
