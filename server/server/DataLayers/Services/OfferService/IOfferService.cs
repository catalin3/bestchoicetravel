﻿using server.DataLayers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DataLayers.Services.OfferService
{
    public interface IOfferService:IService<Offer>
    {
        IEnumerable<Offer> GetOffersByAgent(string agent);
        IEnumerable<Offer> GetOffersByUser(string user);
        IEnumerable<Offer> PaginationFindAll(int pageNumber, int pageSize, string property, string sort);
        IEnumerable<Offer> PaginationFind(int pageNumber, int pageSize);
        IEnumerable<Offer> GetOfferBySearch(string search);
        IEnumerable<Offer> GetLast10();
        IEnumerable<Offer> GetTop10();
        int GetNumberOfOffers();
    }
}
