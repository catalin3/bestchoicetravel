﻿using server.DataLayers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DataLayers.Services.UserService
{
    public interface IUserService:IService<User>
    {
        User Authenticate(string nickname, string password);

        IEnumerable<User> GetAll();
    }
}
