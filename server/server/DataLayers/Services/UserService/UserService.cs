﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using server.DataLayers.Models;
using server.DataLayers.Repositories.UserRepo;

namespace server.DataLayers.Services.UserService
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        private readonly AppSettings _appSettings;

        public UserService(IUserRepository userRepository, IOptions<AppSettings> appSettings)
        {
            this._userRepository = userRepository;
            this._appSettings = appSettings.Value;
        }

        public User Authenticate(string username, string password)
        {
            var user = _userRepository.FindAll().SingleOrDefault(x => x.Username == username && x.Password == password);
            if (user == null)
            {
                return null;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            user.Password = null;

            return user;
        }

        public IEnumerable<User> FindAll()
        {
            return _userRepository.FindAll();
        }

        public IEnumerable<User> GetAll()
        {
            return this.FindAll().Select(x =>
            {
                x.Password = null;
                return x;
            });
        }

        public User FindById(string id)
        {
            return _userRepository.FindById(id);
        }

        public User Insert(User t)
        {
            return _userRepository.Insert(t);
        }

        public void Remove(string id)
        {
            _userRepository.Remove(id);
        }

        public User Update(string id, User t)
        {
            return _userRepository.Update(id, t);
        }
    }
}
