﻿using server.DataLayers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DataLayers.Services.PersonalOfferListService
{
    public interface IPersonalOfferListService:IService<PersonalOfferList>
    {
        IEnumerable<PersonalOfferList> FindByUser(string user);
    }
}
