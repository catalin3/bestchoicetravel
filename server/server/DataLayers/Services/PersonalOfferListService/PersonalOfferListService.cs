﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using server.DataLayers.Models;
using server.DataLayers.Repositories.PersonalOfferListRepo;

namespace server.DataLayers.Services.PersonalOfferListService
{
    public class PersonalOfferListService : IPersonalOfferListService
    {
        private IPersonalOfferListRepository _personalOfferListRepository;

        public PersonalOfferListService(IPersonalOfferListRepository personalOfferListRepository)
        {
            this._personalOfferListRepository = personalOfferListRepository;
        }

        public IEnumerable<PersonalOfferList> FindAll()
        {
            return _personalOfferListRepository.FindAll();
        }

        public PersonalOfferList FindById(string id)
        {
            return _personalOfferListRepository.FindById(id);
        }

        public IEnumerable<PersonalOfferList> FindByUser(string user)
        {
            return _personalOfferListRepository.FindByUser(user);
        }

        public PersonalOfferList Insert(PersonalOfferList t)
        {
            return _personalOfferListRepository.Insert(t);
        }

        public void Remove(string id)
        {
            _personalOfferListRepository.Remove(id);
        }

        public PersonalOfferList Update(string id, PersonalOfferList t)
        {
            return _personalOfferListRepository.Update(id, t);
        }
    }
}
