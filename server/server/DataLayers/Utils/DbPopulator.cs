﻿using MongoDB.Driver;
using server.DataLayers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DataLayers.Utils
{
    public class DbPopulator
    {
        public MongoClient client;
        public IMongoDatabase mongoDatabase;

        public DbPopulator()
        {
            client = new MongoClient();
            mongoDatabase = client.GetDatabase("Licenta");
        }

        public void Populate()
        {
            //AddUser();
            addOffer();
        }

        public void AddUser()
        {
            IMongoCollection<User> users = mongoDatabase.GetCollection<User>("Users");
            User user = new User
            {
                FirstName = "Nelu",
                LastName = "Ion",
                Username = "test",
                Password = "test",
                Email = "nuluionelu@yahoo.com"
            };
            users.InsertOne(user);
        }

        public void addOffer()
        {
            IMongoCollection<Offer> offers = mongoDatabase.GetCollection<Offer>("Offers");
            Offer offer = new Offer
            {
                Type = "Litoral",
                City = "Hurgada"
            };
            offers.InsertOne(offer);
        }
    }
}
