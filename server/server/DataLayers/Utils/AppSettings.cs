﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DataLayers.Services.UserService
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
