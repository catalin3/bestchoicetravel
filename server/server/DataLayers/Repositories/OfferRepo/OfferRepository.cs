﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using server.DataLayers.Models;
using server.DataLayers.Repositories.RepoUtils;

namespace server.DataLayers.Repositories.OfferRepo
{
    public class OfferRepository : IOfferRepository
    {
        private readonly IContext<Offer> _offerContext;

        public OfferRepository()
        {
        }

        public OfferRepository(IContext<Offer> offerContext)
        {
            this._offerContext = offerContext;
        }

        public IEnumerable<Offer> FindAll()
        {
            var emptyFilter = Builders<Offer>.Filter.Eq<string>(p => p.User, null);
            return _offerContext.Entities.FindSync(emptyFilter).ToList();
        }

        public Offer FindById(string id)
        {
            var filter = Builders<Offer>.Filter.Eq<string>(p => p.Id, id);
            return _offerContext.Entities.FindSync(filter).FirstOrDefault();
        }

        public int GetNumberOfOffers()
        {
            var emptyFilter = Builders<Offer>.Filter.Empty;
            return _offerContext.Entities.FindSync(emptyFilter).ToList().Count();
        }

        public IEnumerable<Offer> GetOffersByAgent(string agent)
        {
            var filter = Builders<Offer>.Filter.Eq<string>(p => p.Agent, agent);
            return _offerContext.Entities.FindSync(filter).ToList();
        }

        public IEnumerable<Offer> GetOffersByUser(string user)
        {
            var filter = Builders<Offer>.Filter.Eq<string>(p => p.User, user);
            return _offerContext.Entities.FindSync(filter).ToList();
        }

        public Offer Insert(Offer t)
        {
            _offerContext.Entities.InsertOne(t);
            return FindById(t.Id);
        }

        public IEnumerable<Offer> PaginationFind(int pageNumber, int pageSize)
        {
            var emptyFilter = Builders<Offer>.Filter.Empty;
            return _offerContext.Entities.Find(emptyFilter).Limit(pageSize).Skip(pageNumber * pageSize).ToList();
        }

        public IEnumerable<Offer> PaginationFindAll(int pageNumber, int pageSize, string property, string sort)
        {
            var emptyFilter = Builders<Offer>.Filter.Eq<string>(p => p.User, null);
            if (sort == "Descending")
            {
                if (property == "AccommodationType") { return _offerContext.Entities.Find(emptyFilter).SortByDescending(p => p.AccommodationType).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "Accommodation") { return _offerContext.Entities.Find(emptyFilter).SortByDescending(p => p.Accommodation).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "Country") { return _offerContext.Entities.Find(emptyFilter).SortByDescending(p => p.Country).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "Price") { return _offerContext.Entities.Find(emptyFilter).SortByDescending(p => p.Price).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "Stars") { return _offerContext.Entities.Find(emptyFilter).SortByDescending(p => p.Stars).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "City") { return _offerContext.Entities.Find(emptyFilter).SortByDescending(p => p.City).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
            }
            else
            {
                if (property == "AccommodationType") { return _offerContext.Entities.Find(emptyFilter).SortBy(p => p.AccommodationType).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "Accommodation") { return _offerContext.Entities.Find(emptyFilter).SortBy(p => p.Accommodation).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "Country") { return _offerContext.Entities.Find(emptyFilter).SortBy(p => p.Country).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "Price") { return _offerContext.Entities.Find(emptyFilter).SortBy(p => p.Price).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "Stars") { return _offerContext.Entities.Find(emptyFilter).SortBy(p => p.Stars).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
                if (property == "City") { return _offerContext.Entities.Find(emptyFilter).SortBy(p => p.City).Limit(pageSize).Skip(pageNumber * pageSize).ToList(); }
            }
            return FindAll();
        }

        public void Remove(string id)
        {
            var filter = Builders<Offer>.Filter.Eq<string>(g => g.Id, id);
            _offerContext.Entities.DeleteOne(filter);
        }

        public Offer Update(string id, Offer t)
        {

            //  ObjectId = require('mongodb').ObjectID;
            var filter = Builders<Offer>.Filter.Eq<string>(g => g.Id, id);
            _offerContext.Entities.ReplaceOne(filter, t);
            return FindById(id);
        }
    }
}
