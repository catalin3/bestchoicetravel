﻿using server.DataLayers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DataLayers.Repositories.OfferRepo
{
    public interface IOfferRepository:IRepository<Offer>
    {
        IEnumerable<Offer> GetOffersByAgent(string agent);
        IEnumerable<Offer> GetOffersByUser(string user);
        IEnumerable<Offer> PaginationFindAll(int pageNumber, int pageSize, string property, string sort);
        IEnumerable<Offer> PaginationFind(int pageNumber, int pageSize);
        int GetNumberOfOffers();
    }
}
