﻿using server.DataLayers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DataLayers.Repositories.PersonalOfferListRepo
{
    public interface IPersonalOfferListRepository:IRepository<PersonalOfferList>
    {
        IEnumerable<PersonalOfferList> FindByUser(string user);
    }
}
