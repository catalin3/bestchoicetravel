﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using server.DataLayers.Models;
using server.DataLayers.Repositories.RepoUtils;

namespace server.DataLayers.Repositories.PersonalOfferListRepo
{
    public class PersonalOfferListRepository : IPersonalOfferListRepository
    {
        private IContext<PersonalOfferList> _personalOfferListContext;

        public PersonalOfferListRepository(IContext<PersonalOfferList> context)
        {
            this._personalOfferListContext = context;
        }

        public IEnumerable<PersonalOfferList> FindAll()
        {
            var emptyFilter = Builders<PersonalOfferList>.Filter.Empty;
            return _personalOfferListContext.Entities.FindSync(emptyFilter).ToList();
        }

        public PersonalOfferList FindById(string id)
        {
            var filter = Builders<PersonalOfferList>.Filter.Eq<string>(p => p.Id, id);
            return _personalOfferListContext.Entities.FindSync(filter).FirstOrDefault();
        }

        public IEnumerable<PersonalOfferList> FindByUser(string user)
        {
            var filter = Builders<PersonalOfferList>.Filter.Eq<string>(p => p.User, user);
            return _personalOfferListContext.Entities.FindSync(filter).ToList();
        }

        public PersonalOfferList Insert(PersonalOfferList t)
        {
            _personalOfferListContext.Entities.InsertOne(t);
            return FindById(t.Id);
        }

        public void Remove(string id)
        {
            var filter = Builders<PersonalOfferList>.Filter.Eq<string>(g => g.Id, id);
            _personalOfferListContext.Entities.DeleteOne(filter);
        }

        public PersonalOfferList Update(string id, PersonalOfferList t)
        {
            var filter = Builders<PersonalOfferList>.Filter.Eq<string>(g => g.Id, id);
            _personalOfferListContext.Entities.ReplaceOne(filter, t);
            return FindById(id);
        }
    }
}
