﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DataLayers.Repositories
{
    public interface IRepository<T>
    {
        T Insert(T t);

        IEnumerable<T> FindAll();

        T Update(string id, T t);

        void Remove(string id);

        T FindById(string id);
    }
}
