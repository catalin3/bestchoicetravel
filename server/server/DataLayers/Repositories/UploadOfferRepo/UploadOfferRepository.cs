﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using server.DataLayers.Models;
using server.DataLayers.Repositories.RepoUtils;

namespace server.DataLayers.Repositories.UploadOfferRepo
{
    public class OfferRepository : IOfferRepository
    {
        private readonly IContext<UploadOffer> _uploadOfferContext;

        public OfferRepository(IContext<UploadOffer> upladOfferContext)
        {
            this._uploadOfferContext = upladOfferContext;
        }

        public IEnumerable<UploadOffer> FindAll()
        {
            var emptyFilter = Builders<UploadOffer>.Filter.Empty;
            return _uploadOfferContext.Entities.FindSync(emptyFilter).ToList();
        }

        public UploadOffer FindById(string id)
        {
            var filter = Builders<UploadOffer>.Filter.Eq<string>(p => p.Id, id);
            return _uploadOfferContext.Entities.FindSync(filter).FirstOrDefault();
        }

        public UploadOffer Insert(UploadOffer t)
        {
            _uploadOfferContext.Entities.InsertOne(t);
            return FindById(t.Id);
        }

        public void Remove(string id)
        {
            var filter = Builders<UploadOffer>.Filter.Eq<string>(g => g.Id, id);
            _uploadOfferContext.Entities.DeleteOne(filter);
        }

        public UploadOffer Update(string id, UploadOffer t)
        {
            var filter = Builders<UploadOffer>.Filter.Eq<string>(g => g.Id, id);
            _uploadOfferContext.Entities.ReplaceOne(filter, t);
            return FindById(id);
        }
    }
}
