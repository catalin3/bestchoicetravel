﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using server.DataLayers.Models;
using server.DataLayers.Repositories.RepoUtils;

namespace server.DataLayers.Repositories.UserRepo
{
    public class UserRepository : IUserRepository
    {
        private readonly IContext<User> _userContext;

        public UserRepository(IContext<User> playerContext)
        {
            this._userContext = playerContext;
        }

        public IEnumerable<User> FindAll()
        {
            var emptyFilter = Builders<User>.Filter.Empty;
            return _userContext.Entities.FindSync(emptyFilter).ToList();
        }

        public User FindById(string id)
        {
            var filter = Builders<User>.Filter.Eq<string>(p => p.Id, id);
            return _userContext.Entities.FindSync(filter).FirstOrDefault();
        }

        public User FindByUsernameOrEmail(User user)
        {
            var filter = Builders<User>.Filter.Eq<string>(p => p.Username, user.Username);
            return _userContext.Entities.FindSync(filter).FirstOrDefault();
        }

        public User Insert(User t)
        {
            _userContext.Entities.InsertOne(t);
            return FindById(t.Id);
        }

        public void Remove(string id)
        {
            var filter = Builders<User>.Filter.Eq<string>(g => g.Id, id);
            _userContext.Entities.DeleteOne(filter);
        }

        public User Update(string id, User t)
        {
            var filter = Builders<User>.Filter.Eq<string>(g => g.Id, id);
            _userContext.Entities.ReplaceOne(filter, t);
            return FindById(id);
        }
    }
}
