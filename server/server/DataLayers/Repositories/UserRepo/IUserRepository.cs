﻿using server.DataLayers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DataLayers.Repositories.UserRepo
{
    public interface IUserRepository:IRepository<User>
    {
        User FindByUsernameOrEmail(User user);
    }
}
