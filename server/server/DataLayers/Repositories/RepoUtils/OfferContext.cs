﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using server.DataLayers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DataLayers.Repositories.RepoUtils
{
    public class OfferContext:IContext<Offer>
    {
        private readonly IMongoDatabase _database;

        public OfferContext(IOptions<Settings> options)
        {
            MongoClient client = new MongoClient(options.Value.ConnectionString);
            this._database = client.GetDatabase(options.Value.DatabaseName);
        }

        public IMongoCollection<Offer> Entities
        {
            get
            {
                return _database.GetCollection<Offer>("Offers");
            }
        }
    }
}
