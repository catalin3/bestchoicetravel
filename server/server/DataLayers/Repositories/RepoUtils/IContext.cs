﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DataLayers.Repositories.RepoUtils
{
    public interface IContext<T>
    {
        IMongoCollection<T> Entities { get; }
    }
}
