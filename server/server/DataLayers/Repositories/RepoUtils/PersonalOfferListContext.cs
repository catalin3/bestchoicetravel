﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using server.DataLayers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DataLayers.Repositories.RepoUtils
{
    public class PersonalOfferListContext : IContext<PersonalOfferList>
    {
        private readonly IMongoDatabase _database;

        public PersonalOfferListContext(IOptions<Settings> options)
        {
            var client = new MongoClient(options.Value.ConnectionString);
            this._database = client.GetDatabase(options.Value.DatabaseName);
        }


        public IMongoCollection<PersonalOfferList> Entities
        {
            get
            {
                return _database.GetCollection<PersonalOfferList>("PersonalOfferList");
            }
        }
    }
}
