﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DataLayers.Models
{
    public class UploadOffer
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Type { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Accommodation { get; set; }
        public string AccommodationType { get; set; }
        public string Stars { get; set; }
        public string Price { get; set; }
        public string NumbersOfPersons { get; set; }
        public string Description { get; set; }
    }
}
