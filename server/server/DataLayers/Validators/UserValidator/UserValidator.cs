﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using server.DataLayers.Models;
using server.DataLayers.Repositories.UserRepo;

namespace server.DataLayers.Validators.UserValidator
{
    public class UserValidator : IUserValidator
    {
        private readonly IUserRepository _userRepository;

        public UserValidator(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }

        public bool validate(User t)
        {
            if (exist(t))
            {
                return false;
            }
            return true;    
        }

        public bool exist(User t)
        {
            User user = default(User);
            user = this._userRepository.FindByUsernameOrEmail(t);
            if (user == null)
            {
                return false;
            }
            return true;
        }
    }
}
