﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DataLayers.Validators
{
    public interface IValidator<T>
    {
        Boolean validate(T t); 
    }
}
