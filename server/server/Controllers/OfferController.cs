﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using server.DataLayers.Models;
using server.DataLayers.Services.OfferService;

namespace server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OfferController : ControllerBase
    {
        private readonly IOfferService _offerService;

        public OfferController(IOfferService offerService)
        {
            this._offerService = offerService;
        }

        // GET: api/Offer
        [HttpGet]
        public IActionResult Get([FromQuery] string User, [FromQuery] string Agent, [FromQuery] string Property, [FromQuery] string Sort, [FromQuery] string PageNumber, [FromQuery] string PageSize, [FromQuery] string Count, [FromQuery] string Last, [FromQuery] string Search)
        {
            if (Count != null)
            {
                return Ok(_offerService.GetNumberOfOffers());
            }
            if (PageNumber != null && PageSize != null && Property != null && Sort != null)
            {
                var pageNumber = Int32.Parse(PageNumber);
                var pageSize = Int32.Parse(PageSize);
                return Ok(_offerService.PaginationFindAll(pageNumber, pageSize, Property, Sort));
            }
            if (Agent != null) {
                IEnumerable<Offer> list = _offerService.GetOffersByAgent(Agent);
                return Ok(list); }
            if (User != null)
            {
                IEnumerable<Offer> list = _offerService.GetOffersByUser(User);
                return Ok(list);
            }
            if ("true".Equals(Last))
            {
                IEnumerable<Offer> list = _offerService.GetLast10();
                return Ok(list);
            }
            if ("top".Equals(Last))
            {
                IEnumerable<Offer> list = _offerService.GetTop10();
                return Ok(list);
            }
            if (Search != null)
            {
                return (Ok(_offerService.GetOfferBySearch(Search)));
            }

            return Ok(_offerService.FindAll());
        }

        // POST: api/Offer
        [HttpPost]
        public IActionResult Post([FromBody] Offer offer)
        {
            Offer defaultOffer = default(Offer);
            defaultOffer = _offerService.Insert(offer);
            //if(defaultOffer!= null)
            //{
            //    return Ok(offer);
            //}
            return Ok(defaultOffer);
        }

        // PUT: api/Offer/5
        [HttpPut("{id}")]
        public IActionResult Put(string id, [FromBody] Offer offer)
        {
            Offer defaultOffer = default(Offer);
            defaultOffer = _offerService.Update(id, offer);
            return Ok(defaultOffer);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            _offerService.Remove(id);
            Offer defaultOffer = default(Offer);
            defaultOffer = _offerService.FindById(id);
            if(defaultOffer != null)
            {
                return Ok("Offer removed!!");
            }
            return Ok("Error to remove Offer!!");
        }
    }
}
