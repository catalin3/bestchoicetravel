﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using server.DataLayers.Models;
using server.DataLayers.Services.UserService;
using server.DataLayers.Validators;

namespace server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserCrudController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IUserValidator _userValidator;

        public UserCrudController(IUserService userService, IUserValidator userValidator)
        {
            this._userService = userService;
            this._userValidator = userValidator;
        }

        // GET: api/UserCrud
        [HttpGet]
        public IActionResult Get([FromQuery] string Id)
        {
            if(Id != null)
            {
                return Ok(_userService.FindById(Id));
            }
            return Ok(_userService.FindAll());
        }

        // POST: api/UserCrud
        [HttpPost]
        public IActionResult Post([FromBody] User value)
        {
            if (!_userValidator.validate(value))
            {
                return BadRequest(new { message = "User is not valid. Try with other Username!!" });
            }
            User user = default(User);
            user = _userService.Insert(value);
            if(user != null)
            {
                return Ok("User added!!");
            }
            return BadRequest(new { message = "Error in add User!!" });
        }

        // PUT: api/UserCrud/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
