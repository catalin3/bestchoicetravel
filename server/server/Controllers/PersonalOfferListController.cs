﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using server.DataLayers.Models;
using server.DataLayers.Services.PersonalOfferListService;

namespace server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonalOfferListController : ControllerBase
    {
        private readonly IPersonalOfferListService _personalOfferListService;

        public PersonalOfferListController(IPersonalOfferListService personalOfferListService)
        {
            this._personalOfferListService = personalOfferListService;
        }

        // GET: api/Offer
        [HttpGet]
        public IActionResult Get([FromQuery] string User)
        {
            if (User != null)
            {
                IEnumerable<PersonalOfferList> list = _personalOfferListService.FindByUser(User);
                return Ok(list);
            }

            return Ok(_personalOfferListService.FindAll());
        }

        // POST: api/Offer
        [HttpPost]
        public IActionResult Post([FromBody] PersonalOfferList offer)
        {
            PersonalOfferList defaultOffer = default(PersonalOfferList);
            defaultOffer = _personalOfferListService.Insert(offer);
            //if(defaultOffer!= null)
            //{
            //    return Ok(offer);
            //}
            return Ok(defaultOffer);
        }

        // PUT: api/Offer/5
        [HttpPut("{id}")]
        public IActionResult Put(string id, [FromBody] PersonalOfferList offer)
        {
            PersonalOfferList defaultOffer = default(PersonalOfferList);
            defaultOffer = _personalOfferListService.Update(id, offer);
            return Ok(defaultOffer);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            _personalOfferListService.Remove(id);
            PersonalOfferList defaultOffer = default(PersonalOfferList);
            defaultOffer = _personalOfferListService.FindById(id);
            if (defaultOffer != null)
            {
                return Ok("Offer removed!!");
            }
            return Ok("Error to remove Offer!!");
        }
    }
}
